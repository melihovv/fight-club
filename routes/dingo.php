<?php

use Dingo\Api\Routing\Router;

$api = app(Dingo\Api\Routing\Router::class);

$api->version('v1', [
    'middleware' => ['api'],
    'namespace' => 'App\Http\Controllers\Api',
], function (Router $api) {
    $api->post('login', [
        'uses' => 'Auth\AuthController@login',
        'as' => 'login',
    ]);
    $api->post('register', [
        'uses' => 'Auth\AuthController@register',
        'as' => 'register',
    ]);

    $api->resource('roles', 'RolesController', [
        'only' => ['index'],
    ]);

    $api->group(['middleware' => ['api.auth']], function (Router $api) {
        $api->get('user', [
            'uses' => 'Auth\AuthController@user',
            'as' => 'user',
        ]);

        $api->get('balance', [
            'uses' => 'BalanceController@get',
            'as' => 'balance.get',
        ]);
        $api->post('balance/fill-up', [
            'uses' => 'BalanceController@fillUp',
            'as' => 'balance.fill-up',
        ]);

        $api->get('fight-club', [
            'uses' => 'FightClubOwnerController@get',
            'as' => 'fight-club.get',
        ]);
        $api->put('fight-club', [
            'uses' => 'FightClubOwnerController@update',
            'as' => 'fight-club.update',
        ]);
        $api->post('fight-club/exclude/{user}', [
            'uses' => 'FightClubOwnerController@exclude',
            'as' => 'fight-club.exclude',
        ]);
        $api->post('fight-club/exclude-from-blacklist/{user}', [
            'uses' => 'FightClubOwnerController@excludeFromBlackList',
            'as' => 'fight-club.exclude-from-blacklist',
        ]);
        $api->post('fight-club/fine/{user}', [
            'uses' => 'FightClubOwnerController@fine',
            'as' => 'fight-club.fine',
        ]);

        $api->get('fight-clubs', [
            'uses' => 'FightClubsController@list',
            'as' => 'fight-clubs',
        ]);
        $api->post('fight-clubs/{fight_club}/join', [
            'uses' => 'FightClubsController@join',
            'as' => 'fight-clubs.join',
        ]);
        $api->post('fight-clubs/{fight_club}/leave', [
            'uses' => 'FightClubsController@leave',
            'as' => 'fight-clubs.leave',
        ]);

        $api->get('users/{user}', [
            'uses' => 'UsersController@get',
            'as' => 'users.get',
        ]);

        $api->get('payments', [
            'uses' => 'PaymentsController@get',
            'as' => 'payments.get',
        ]);
        $api->post('payments/pay/{payment}', [
            'uses' => 'PaymentsController@pay',
            'as' => 'payments.pay',
        ]);

        $api->put('profile', [
            'uses' => 'ProfileController@update',
            'as' => 'profile.update',
        ]);

        $api->get('products', [
            'uses' => 'ProductsController@index',
            'as' => 'products.index',
        ]);
        $api->post('products/{product}', [
            'uses' => 'ProductsController@buy',
            'as' => 'products.buy',
        ]);
        $api->get('products/my', [
            'uses' => 'ProductsController@my',
            'as' => 'products.my',
        ]);
        $api->post('products', [
            'uses' => 'ProductsController@add',
            'as' => 'products.add',
        ]);
        $api->get('products/{product}', [
            'uses' => 'ProductsController@get',
            'as' => 'products.get',
        ]);
        $api->delete('products/{product}', [
            'uses' => 'ProductsController@destroy',
            'as' => 'products.destroy',
        ]);
        $api->put('products/{product}', [
            'uses' => 'ProductsController@update',
            'as' => 'products.update',
        ]);

        $api->get('meetings', [
            'uses' => 'MeetingsController@index',
            'as' => 'meetings.index',
        ]);
        $api->post('meetings', [
            'uses' => 'MeetingsController@add',
            'as' => 'meetings.add',
        ]);
        $api->get('meetings/member', [
            'uses' => 'MeetingsController@member',
            'as' => 'meetings.member',
        ]);
        $api->delete('meetings/{meeting}', [
            'uses' => 'MeetingsController@destroy',
            'as' => 'meetings.destroy',
        ]);
    });
});
