<?php

declare(strict_types = 1);

if (!function_exists('get_current_action')) {
    /**
     * @return string
     */
    function get_current_action() : string
    {
        list(, $action) = explode(
            '@',
            Route::getCurrentRoute()->getActionName()
        );

        return $action;
    }
}

if (!function_exists('strf_time')) {
    function strf_time($format, $timestamp, $locale)
    {
        $dateStr = strftime($format, $timestamp);

        if (strpos($locale, '1251') !== false) {
            return iconv('cp1251', 'utf-8', $dateStr);
        } else {
            return $dateStr;
        }
    }
}
