<?php

declare(strict_types = 1);

namespace App\Console\Commands;

use App\Models\Payment;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class InvoiceClubMembers extends Command
{
    protected $signature = 'invoice:members';

    protected $description = 'Invoice fight clubs members';

    public function handle()
    {
        $data = DB::table('fight_clubs_members')
            ->join(
                'fight_clubs',
                'fight_clubs_members.fight_club_id',
                '=',
                'fight_clubs.id'
            )
            ->join(
                'users',
                'fight_clubs.owner',
                '=',
                'users.id'
            )
            ->select([
                'users.id' => 'owner',
                'fight_clubs.name',
                'fight_clubs.membership_price',
                'fight_clubs_members.member',
                'fight_clubs_members.created_at',
            ])
            ->get();

        $localeTime = setlocale(LC_TIME, 'ru_RU.UTF-8', 'Rus');
        $month = mb_strtolower(strf_time('%B', time(), $localeTime));

        $now = Carbon::now()->startOfDay();
        for ($i = 0, $len = $data->count(); $i < $len; ++$i) {
            $then = Carbon::createFromFormat(
                'Y-m-d H:i:s',
                $data[$i]->created_at
            )
                ->startOfDay();

            $diff = $now->diff($then);

            if ($diff->m > 0 && $diff->d === 0) {
                $clubname = $data[$i]->name;

                Payment::create([
                    'from' => $data[$i]->member,
                    'to' => $data[$i]->owner,
                    'amount' => $data[$i]->membership_price,
                    'comment' => "Оплата членства в клубе $clubname за " .
                        "$month месяц",
                    'type' => 'membership',
                ]);
            }
        }
    }
}
