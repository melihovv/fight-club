<?php

declare(strict_types = 1);

namespace App\Models;

use Laratrust\LaratrustPermission;

class Permission extends LaratrustPermission
{
    use AdditionalMethods;
}
