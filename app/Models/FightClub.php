<?php

declare(strict_types = 1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class FightClub extends Model
{
    use AdditionalMethods;

    protected $fillable = [
        'owner',
        'name',
        'membership_price',
    ];

    protected $casts = [
        'owner' => 'integer',
        'membership_price' => 'double',
    ];

    public function meetings() : HasMany
    {
        return $this->hasMany(Meeting::class);
    }

    public function members() : BelongsToMany
    {
        return $this
            ->belongsToMany(
                User::class,
                'fight_clubs_members',
                null,
                'member'
            )
            ->withTimestamps();
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner');
    }

    public function blacklistedUsers() : BelongsToMany
    {
        return $this->belongsToMany(User::class, 'fight_club_blacklist');
    }
}
