<?php

declare(strict_types = 1);

namespace App\Models;

use Laratrust\LaratrustRole;

class Role extends LaratrustRole
{
    use AdditionalMethods;
}
