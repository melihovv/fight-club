<?php

declare(strict_types = 1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Product extends Model
{
    use AdditionalMethods;

    protected $fillable = [
        'seller',
        'price',
        'name',
        'description',
        'type',
        'attributes',
    ];

    protected $casts = [
        'seller' => 'integer',
        'price' => 'integer',
        'type' => 'string',
        'attributes' => 'array',
    ];

    public function sellerUser() : BelongsTo
    {
        return $this->belongsTo(User::class, 'seller');
    }

    public function meetings() : HasMany
    {
        return $this->hasMany(Meeting::class);
    }
}
