<?php

declare(strict_types = 1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Meeting extends Model
{
    use AdditionalMethods;

    protected $fillable = [
        'fight_club_id',
        'product_id',
        'at',
    ];

    protected $casts = [
        'fight_club_id' => 'integer',
        'product_id' => 'integer',
        'at' => 'datetime',
    ];

    public function fightClub() : BelongsTo
    {
        return $this->belongsTo(FightClub::class);
    }

    public function product() : BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
}
