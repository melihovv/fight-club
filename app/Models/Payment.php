<?php

declare(strict_types = 1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Payment extends Model
{
    use AdditionalMethods;

    protected $fillable = [
        'from',
        'to',
        'amount',
        'comment',
        'type',
        'paid',
    ];

    protected $casts = [
        'from' => 'integer',
        'to' => 'integer',
        'amount' => 'integer',
        'type' => 'string',
        'paid' => 'boolean',
    ];

    public function fromUser() : BelongsTo
    {
        return $this->belongsTo(User::class, 'from');
    }

    public function toUser() : BelongsTo
    {
        return $this->belongsTo(User::class, 'to');
    }
}
