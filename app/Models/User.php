<?php

declare(strict_types = 1);

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;
    use AdditionalMethods;

    protected $fillable = [
        'name',
        'email',
        'password',
        'balance',
        'attributes',
    ];

    protected $hidden = [
        'password',
        'remember_token',
        'attributes',
    ];

    protected $casts = [
        'balance' => 'double',
        'attributes' => 'array',
    ];

    public function meetings() : HasManyThrough
    {
        return $this->hasManyThrough(Meeting::class, FightClub::class, 'owner');
    }

    public function advertisements() : HasMany
    {
        return $this->hasMany(Product::class, 'publisher');
    }

    public function fightClubs() : BelongsToMany
    {
        return $this
            ->belongsToMany(
                FightClub::class,
                'fight_clubs_members',
                'member'
            )
            ->withTimestamps();
    }

    public function ownedFightClub() : HasOne
    {
        return $this->hasOne(FightClub::class, 'owner');
    }

    public function fightClubsInBlacklist() : BelongsToMany
    {
        return $this->belongsToMany(FightClub::class, 'fight_club_blacklist');
    }

    public function myPayments() : HasMany
    {
        return $this->hasMany(Payment::class, 'from');
    }

    public function paymentsToMe() : HasMany
    {
        return $this->hasMany(Payment::class, 'to');
    }
}
