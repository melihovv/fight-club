<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Api;

use App\Http\Transformers\FightClubTransformer;
use App\Models\FightClub;

class FightClubsController extends ApiController
{
    public function __construct()
    {
        $this->middleware('permission:join-leave-club', [
            'only' => [
                'join',
                'leave',
            ],
        ]);
    }

    public function list()
    {
        $fightClubs = FightClub::withCount('members')->get();
        $joinedFightClubs = $this->auth->user()->fightClubs()->pluck('id');

        return $this->response
            ->collection($fightClubs, new FightClubTransformer())
            ->addMeta('joined_fight_clubs', $joinedFightClubs);
    }

    public function join(FightClub $fightClub)
    {
        $blacklistedUserIds = $fightClub->blacklistedUsers()->pluck('user_id')
            ->toArray();

        if (in_array($this->auth->user()->id, $blacklistedUserIds, false)) {
            return $this->response->errorBadRequest(
                "Вы не можете вступить в клуб '$fightClub->name', " .
                'потому что организатор этого клуба вас исключил'
            );
        }

        $fightClub->members()->attach($this->auth->user()->id);

        return $this->response->noContent();
    }

    public function leave(FightClub $fightClub)
    {
        $fightClub->members()->detach($this->auth->user()->id);

        return $this->response->noContent();
    }
}
