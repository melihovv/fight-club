<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Api;

use App\Http\Transformers\UserTransformer;
use App\Models\User;

class UsersController extends ApiController
{
    public function get(User $user)
    {
        return $this->response->item($user, new UserTransformer());
    }
}
