<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Api;

use App\Http\Transformers\PaymentTransformer;
use App\Models\Payment;
use Illuminate\Support\Facades\DB;
use PDOException;

class PaymentsController extends ApiController
{
    public function __construct()
    {
        $this->middleware('role:fight-clubs-member', [
            'only' => [
                'get',
                'pay',
            ],
        ]);
    }

    public function get()
    {
        return $this->response->collection(
            $this->auth->user()->myPayments()->where('paid', false)->get(),
            new PaymentTransformer()
        );
    }

    public function pay(Payment $payment)
    {
        $user = $this->auth->user();

        if ($payment->from !== $user->id) {
            return $this->response->errorBadRequest(
                'Вы пытаетесь оплатить не свой счет'
            );
        }

        if ($payment->amount > $user->balance) {
            return $this->response->errorBadRequest(
                'У вас недостаточно средств для оплаты этого счета'
            );
        }

        if ($payment->paid) {
            return $this->response->errorBadRequest('Этот счет уже оплачен');
        }

        try {
            DB::beginTransaction();

            $payment->update(['paid' => true]);

            $toUser = $payment->toUser;
            $fromUser = $payment->fromUser;

            $toUser->update(['balance' => $toUser->balance + $payment->amount]);
            $fromUser->update([
                'balance' => $fromUser->balance - $payment->amount,
            ]);

            DB::commit();
        } catch (PDOException $e) {
            DB::rollback();
            return $this->response->errorInternal();
        }

        return $this->response->noContent();
    }
}
