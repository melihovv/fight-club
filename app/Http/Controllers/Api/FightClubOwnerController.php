<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Api;

use App\Http\Requests\FightClubOwnerFineRequest;
use App\Http\Requests\FightClubOwnerUpdateRequest;
use App\Http\Transformers\FightClubTransformer;
use App\Models\Payment;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use PDOException;

class FightClubOwnerController extends ApiController
{
    public function __construct()
    {
        $this->middleware('permission:manage-fight-club');
    }

    /**
     * @return \Dingo\Api\Http\Response
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function get()
    {
        return $this->response->item(
            $this->auth->user()->ownedFightClub,
            new FightClubTransformer(),
            [],
            function ($resource, $fractal) {
                $fractal->parseIncludes(['members', 'banned_users']);
            }
        );
    }

    public function update(FightClubOwnerUpdateRequest $request)
    {
        $user = $this->auth->user();

        $user->ownedFightClub->update(array_merge($request->all(), [
            'owner' => $user->id,
        ]));

        return $this->response->noContent();
    }

    public function exclude(User $user)
    {
        $fightClub = $this->auth->user()->ownedFightClub;

        try {
            DB::beginTransaction();
            $fightClub->blacklistedUsers()->sync([$user->id], false);
            $fightClub->members()->detach($user->id);
            DB::commit();
        } catch (PDOException $e) {
            DB::rollback();

            return $this->response->errorInternal();
        }

        return $this->response->noContent();
    }

    public function excludeFromBlackList(User $user)
    {
        $this->auth->user()->ownedFightClub
            ->blacklistedUsers()->detach($user->id);

        return $this->response->noContent();
    }

    public function fine(User $user, FightClubOwnerFineRequest $request)
    {
        $membersIds = $this->auth->user()->ownedFightClub->members()
            ->pluck('id')->toArray();

        if (!in_array($user->id, $membersIds, false)) {
            return $this->response->errorBadRequest(
                "Вы не можете выписывать штрафы пользователю $user->name, " .
                'так как он не состоит в вашем бойцовском клубе'
            );
        }

        Payment::create([
            'from' => $user->id,
            'to' => $this->auth->user()->id,
            'amount' => $request->get('amount'),
            'comment' => $request->get('comment'),
            'type' => 'fine',
        ]);

        return $this->response->noContent();
    }
}
