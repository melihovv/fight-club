<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Api;

use App\Http\Requests\ProfileUpdateRequest;
use Illuminate\Support\Facades\Hash;

class ProfileController extends ApiController
{
    public function update(ProfileUpdateRequest $request)
    {
        $user = $this->auth->user();
        if (!Hash::check($request->get('old_password'), $user->password)) {
            return $this->response->errorBadRequest(
                'Старый пароль введен неверно'
            );
        }

        $attrs = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
        ];

        if ($user->hasRole('fight-club-owner')) {
            $attrs['attributes'] = [
                'goal' => $request->get('goal'),
            ];
        }

        $user->update($attrs);

        return $this->response->noContent();
    }
}
