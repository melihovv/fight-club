<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Api;

use App\Http\Requests\MeetingsAddRequest;
use App\Http\Transformers\MeetingTransformer;
use App\Models\Meeting;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use PDOException;

class MeetingsController extends ApiController
{
    public function __construct()
    {
        $this->middleware('permission:manage-fight-club', [
            'only' => [
                'index',
                'destroy',
                'add',
            ],
        ]);
        $this->middleware('role:fight-clubs-member', [
            'only' => [
                'member',
            ],
        ]);
    }

    public function index()
    {
        return $this->response->collection(
            $this->auth->user()->ownedFightClub->meetings,
            new MeetingTransformer()
        );
    }

    public function destroy(Meeting $meeting)
    {
        $meeting->delete();

        return $this->response->noContent();
    }

    public function add(MeetingsAddRequest $request)
    {
        $user = $this->auth->user();
        $product = Product::find($request->get('product_id'));

        if ($product->price > $user->balance) {
            return $this->response->errorBadRequest(
                'У вас недостаточно средств для оплаты'
            );
        }

        try {
            DB::beginTransaction();

            $user->update([
                'balance' => $user->balance - $product->price,
            ]);
            $seller = $product->sellerUser;
            $seller->update(['balance' => $seller->balance + $product->price]);

            Meeting::create(array_merge($request->all(), [
                'fight_club_id' => $user->ownedFightClub->id,
            ]));

            DB::commit();
        } catch (PDOException $e) {
            DB::rollback();

            return $this->response->errorInternal();
        }

        return $this->response->noContent();
    }

    /**
     * @return \Dingo\Api\Http\Response
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function member()
    {
        $user = $this->auth->user();
        $fightClubsIds = $user->fightClubs()->pluck('id')->toArray();

        return $this->response->collection(
            Meeting::whereIn('fight_club_id', $fightClubsIds)
                ->where('at', '>', Carbon::now())->get(),
            new MeetingTransformer(),
            [],
            function ($resource, $fractal) {
                $fractal->parseIncludes(['fightClub']);
            }
        );
    }
}
