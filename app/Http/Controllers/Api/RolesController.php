<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Api;

use App\Http\Transformers\RoleTransformer;
use App\Models\Role;

class RolesController extends ApiController
{
    public function index()
    {
        $roles = Role::all();

        return $this->response->collection($roles, new RoleTransformer());
    }
}
