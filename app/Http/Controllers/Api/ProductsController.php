<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Api;

use App\Http\Requests\ProductsAddUpdateRequest;
use App\Http\Requests\ProductsIndexMyRequest;
use App\Http\Transformers\ProductTransformer;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use PDOException;

class ProductsController extends ApiController
{
    public function __construct()
    {
        $this->middleware('permission:buy', [
            'only' => [
                'index',
                'buy',
            ],
        ]);
        $this->middleware('permission:sell', [
            'only' => [
                'add',
                'destroy',
                'update',
            ],
        ]);
    }

    public function index(ProductsIndexMyRequest $request)
    {
        return $this->response->collection(
            Product::whereType($request->get('type'))->get(),
            new ProductTransformer()
        );
    }

    public function buy(Product $product)
    {
        $user = $this->auth->user();

        if ($product->price > $user->balance) {
            return $this->response->errorBadRequest(
                'У вас недостаточно средств для оплаты'
            );
        }

        try {
            DB::beginTransaction();

            $seller = $product->sellerUser;
            $attributes = [];

            if ($seller->hasRole('bomb-seller')) {
                $bombsWeight = $user->attributes['bombs_weight'] ?? 0;
                $attributes = array_merge($user->attributes, [
                    'bombs_weight' => $product->attributes['weight']
                        + $bombsWeight,
                ]);
            }

            $user->update([
                'balance' => $user->balance - $product->price,
                'attributes' => $attributes,
            ]);
            $seller->update(['balance' => $seller->balance + $product->price]);

            DB::commit();
        } catch (PDOException $e) {
            DB::rollback();

            return $this->response->errorInternal();
        }

        return $this->response->noContent();
    }

    public function get(Product $product)
    {
        return $this->response->item($product, new ProductTransformer());
    }

    public function my(ProductsIndexMyRequest $request)
    {
        return $this->response->collection(
            Product::whereType($request->get('type'))
                ->whereSeller($this->auth->user()->id)->get(),
            new ProductTransformer()
        );
    }

    public function add(ProductsAddUpdateRequest $request)
    {
        $user = $this->auth->user();

        Product::create(array_merge($request->all(), [
            'seller' => $user->id,
            'type' => $user->hasRole('bomb-seller') ? 'bomb' : 'accommodation',
            'attributes' => [
                'weight' => $request->get('weight'),
                'address' => $request->get('address'),
            ],
        ]));

        return $this->response->noContent();
    }

    public function destroy(Product $product)
    {
        $product->delete();

        return $this->response->noContent();
    }

    public function update(Product $product, ProductsAddUpdateRequest $request)
    {
        $product->update(array_merge($request->all(), [
            'seller' => $this->auth->user()->id,
            'attributes' => [
                'weight' => $request->get('weight'),
                'address' => $request->get('address'),
            ],
        ]));

        return $this->response->noContent();
    }
}
