<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\AuthLoginRequest;
use App\Http\Requests\AuthRegisterRequest;
use App\Http\Transformers\UserTransformer;
use App\Models\FightClub;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use PDOException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends ApiController
{
    /**
     * @param AuthLoginRequest $request
     * @return \Dingo\Api\Http\Response|void
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function login(AuthLoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return $this->response->errorUnauthorized();
            }

            $user = JWTAuth::toUser($token);

            return $this->response
                ->item(
                    $user,
                    new UserTransformer(),
                    [],
                    function ($resource, $fractal) {
                        $fractal->parseIncludes('roles');
                    }
                )
                ->addMeta('token', $token);
        } catch (JWTException $e) {
            return $this->response->errorInternal();
        }
    }

    public function register(AuthRegisterRequest $request)
    {
        try {
            DB::beginTransaction();

            $role = Role::find($request->get('role'));
            $isFightClubOwner = $role->name === 'fight-club-owner';

            $user = User::create(array_merge([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => bcrypt($request->get('password')),
                'balance' => $request->get('balance'),
            ], $isFightClubOwner ? [
                'attributes' => [
                    'goal' => $request->get('goal'),
                    'bombs_weight' => 0,
                ],
            ] : []));
            $user->roles()->attach($request->get('role'));

            if ($isFightClubOwner) {
                FightClub::create([
                    'owner' => $user->id,
                    'name' => $request->get('fight_club_name'),
                    'membership_price' => $request->get('membership_price'),
                ]);
            }

            DB::commit();
        } catch (PDOException $e) {
            DB::rollback();

            return $this->response->errorInternal('Something was wrong');
        }

        return $this->api->raw()->post('login', [
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        ]);
    }

    /**
     * @return \Dingo\Api\Http\Response
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function user()
    {
        return $this->response->item(
            $this->auth->user(),
            new UserTransformer(),
            [],
            function ($resource, $fractal) {
                $fractal->parseIncludes('roles');
            }
        );
    }
}
