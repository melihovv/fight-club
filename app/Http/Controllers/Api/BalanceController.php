<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Api;

use App\Http\Requests\BalanceFillUpRequest;

class BalanceController extends ApiController
{
    public function __construct()
    {
        $this->middleware('permission:view-balance', ['only' => ['get']]);
        $this->middleware('permission:manage-balance', ['only' => ['fillUp']]);
    }

    public function get()
    {
        $user = $this->auth->user();

        return $this->response->array(['balance' => $user->balance]);
    }

    public function fillUp(BalanceFillUpRequest $request)
    {
        $user = $this->auth->user();
        $user->update(['balance' => $user->balance + $request->get('amount')]);

        return $this->response->noContent();
    }
}
