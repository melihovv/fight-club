<?php

declare(strict_types = 1);

namespace App\Http\Transformers;

use App\Models\Permission;
use League\Fractal\TransformerAbstract;

class PermissionTransformer extends TransformerAbstract
{
    public function transform(Permission $model) : array
    {
        return [
            'id' => $model->id,
            'name' => $model->name,
            'display_name' => $model->display_name,
            'description' => $model->description,
        ];
    }
}
