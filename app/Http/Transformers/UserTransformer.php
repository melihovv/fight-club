<?php

declare(strict_types = 1);

namespace App\Http\Transformers;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'roles',
    ];

    public function transform(User $model) : array
    {
        return [
            'id' => $model->id,
            'name' => $model->name,
            'email' => $model->email,
            'balance' => $model->balance,
            'attributes' => $model->attributes,
        ];
    }

    public function includeRoles(User $model)
    {
        return $this->collection($model->roles, new RoleTransformer());
    }
}
