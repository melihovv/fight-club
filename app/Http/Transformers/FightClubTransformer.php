<?php

declare(strict_types = 1);

namespace App\Http\Transformers;

use App\Models\FightClub;
use League\Fractal\TransformerAbstract;

class FightClubTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'members',
        'banned_users',
    ];

    public function transform(FightClub $model) : array
    {
        $attrs = [
            'id' => $model->id,
            'owner' => $model->owner,
            'name' => $model->name,
            'membership_price' => $model->membership_price,
        ];

        if (isset($model->members_count)) {
            $attrs['members_count'] = $model->members_count;
        }

        return $attrs;
    }

    public function includeMembers(FightClub $model)
    {
        return $this->collection($model->members, new UserTransformer());
    }

    public function includeBannedUsers(FightClub $model)
    {
        return $this->collection(
            $model->blacklistedUsers,
            new UserTransformer()
        );
    }
}
