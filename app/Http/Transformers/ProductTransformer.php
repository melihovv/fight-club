<?php

declare(strict_types = 1);

namespace App\Http\Transformers;

use App\Models\Product;
use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract
{
    public function transform(Product $model) : array
    {
        return [
            'id' => $model->id,
            'name' => $model->name,
            'description' => $model->description,
            'seller' => $model->seller,
            'price' => $model->price,
            'type' => $model->type,
            'attributes' => $model->attributes,
        ];
    }
}
