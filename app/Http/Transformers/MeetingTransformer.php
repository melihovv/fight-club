<?php

declare(strict_types = 1);

namespace App\Http\Transformers;

use App\Models\Meeting;
use League\Fractal\TransformerAbstract;

class MeetingTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'fightClub',
    ];

    public function transform(Meeting $model) : array
    {
        return [
            'id' => $model->id,
            'fight_club_id' => $model->fight_club_id,
            'product_id' => $model->product_id,
            'address' => $model->product->attributes['address'],
            'at' => $model->at->format('Y-m-d H:i:s'),
        ];
    }

    public function includeFightClub(Meeting $model)
    {
        return $this->item($model->fightClub, new FightClubTransformer());
    }
}
