<?php

declare(strict_types = 1);

namespace App\Http\Transformers;

use App\Models\Payment;
use League\Fractal\TransformerAbstract;

class PaymentTransformer extends TransformerAbstract
{
    public function transform(Payment $model) : array
    {
        return [
            'id' => $model->id,
            'from' => $model->from,
            'to' => $model->to,
            'amount' => $model->amount,
            'comment' => $model->comment,
            'type' => $model->type,
            'paid' => $model->paid,
        ];
    }
}
