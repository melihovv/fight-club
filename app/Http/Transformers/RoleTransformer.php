<?php

declare(strict_types = 1);

namespace App\Http\Transformers;

use App\Models\Role;
use League\Fractal\TransformerAbstract;

class RoleTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'permissions',
    ];

    public function transform(Role $model) : array
    {
        return [
            'id' => $model->id,
            'name' => $model->name,
            'display_name' => $model->display_name,
            'description' => $model->description,
        ];
    }

    public function includePermissions(Role $model)
    {
        return $this->collection(
            $model->permissions,
            new PermissionTransformer()
        );
    }
}
