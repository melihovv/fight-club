<?php

declare(strict_types = 1);

namespace App\Http\Transformers;

use App\Models\Advertisement;
use League\Fractal\TransformerAbstract;

class AdvertisementTransformer extends TransformerAbstract
{
    public function transform(Advertisement $model) : array
    {
        return [
            'id' => $model->id,
            'publisher' => $model->user_id,
            'content' => $model->content,
        ];
    }
}
