<?php

declare(strict_types = 1);

namespace App\Http\Requests;

class FightClubOwnerFineRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'amount' => 'required|min:1',
            'comment' => 'max:255',
            'type' => 'in:fine'
        ];
    }

    public function attributes()
    {
        return [
            'amount' => 'сумма',
            'comment' => 'причина',
        ];
    }
}
