<?php

declare(strict_types = 1);

namespace App\Http\Requests;

use App\Models\Role;
use Illuminate\Validation\Rule;

class AuthRegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => [
                'required',
                'email',
                'max:255',
                Rule::unique('users'),
            ],
            'password' => 'required|min:6',
            'balance' => 'required|numeric|min:0',
            'role' => [
                'required',
                'integer',
                'min:1',
                Rule::exists('roles', 'id'),
            ],
        ];
    }

    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        $role = $this->validationData()['role'] ?? 0;
        if (!$role) {
            return $validator;
        }

        $role = Role::find($role);
        if (!$role) {
            return $validator;
        }

        if ($role->name === 'fight-club-owner') {
            $validator->sometimes(
                'fight_club_name',
                [
                    'required',
                    'max:255',
                    Rule::unique('fight_clubs', 'name')
                ],
                function ($input) {
                    return true;
                }
            );

            $validator->sometimes(
                ['goal', 'membership_price'],
                [
                    'required',
                    'numeric',
                    'min:0',
                ],
                function ($input) {
                    return true;
                }
            );
        }

        return $validator;
    }

    public function attributes()
    {
        return [
            'name' => 'никнейм',
        ];
    }
}
