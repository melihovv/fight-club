<?php

declare(strict_types = 1);

namespace App\Http\Requests;

class ProductsAddUpdateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'price' => 'required|numeric|min:1',
            'name' => 'required|max:255',
            'type' => 'required|in:accommodation,bomb',
        ];
    }

    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        $type = $this->validationData()['type'] ?? '';

        if ($type === 'bomb') {
            $validator->sometimes(
                'weight',
                [
                    'required',
                    'numeric',
                    'min:0'
                ],
                function ($input) {
                    return true;
                }
            );
        } elseif ($type === 'accommodation') {
            $validator->sometimes(
                'address',
                [
                    'required',
                    'max:255'
                ],
                function ($input) {
                    return true;
                }
            );
        }

        return $validator;
    }

    public function attributes()
    {
        return [
            'name' => 'название продукта',
            'type' => 'тип продукта',
            'weight' => 'вес',
            'address' => 'адрес',
        ];
    }
}
