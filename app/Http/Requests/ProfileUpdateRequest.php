<?php

declare(strict_types = 1);

namespace App\Http\Requests;

use Dingo\Api\Auth\Auth;
use Illuminate\Validation\Rule;

class ProfileUpdateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => [
                'required',
                'email',
                'max:255',
                Rule::unique('users')->ignore(app(Auth::class)->user()->id),
            ],
            'old_password' => 'required|min:6',
            'password' => 'required|min:6',
        ];
    }

    /**
     * @return \Illuminate\Contracts\Validation\Validator
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        if (app(Auth::class)->user()->hasRole('fight-club-owner')) {
            $validator->sometimes(
                'goal',
                [
                    'required',
                    'numeric',
                    'min:0',
                ],
                function ($input) {
                    return true;
                }
            );
        }

        return $validator;
    }

    public function attributes()
    {
        return [
            'name' => 'никнейм',
            'old_password' => 'старый пароль',
        ];
    }
}
