<?php

declare(strict_types = 1);

namespace App\Http\Requests;

class ProductsIndexMyRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'type' => 'required|in:accommodation,bomb',
        ];
    }

    public function attributes()
    {
        return [
            'type' => 'тип продукта',
        ];
    }
}
