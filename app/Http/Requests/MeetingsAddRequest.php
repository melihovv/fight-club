<?php

declare(strict_types = 1);

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class MeetingsAddRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'product_id' => [
                'required',
                'numeric',
                'min:1',
                Rule::exists('products', 'id'),
            ],
            'at' => 'required|date_format:Y-m-d H:i:s',
        ];
    }

    public function attributes()
    {
        return [
            'product_id' => 'идентификатор помещения',
            'at' => 'дата собрания',
        ];
    }
}
