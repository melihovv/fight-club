<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Models\Meeting;
use App\Models\Product;
use App\Models\FightClub;
use App\Models\Payment;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Faker\Generator;
use Illuminate\Database\Eloquent\Factory;

/**
 * @var Factory $factory
 */
$factory->define(User::class, function (Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'balance' => $faker->randomNumber(),
    ];
});

$factory->define(Role::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->word,
        'display_name' => $faker->sentence(),
        'description' => $faker->sentence(),
    ];
});

$factory->define(Permission::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->word,
        'display_name' => $faker->sentence(),
        'description' => $faker->sentence(),
    ];
});

$factory->define(Product::class, function (Generator $faker) {
    static $types = ['accommodation', 'bomb'];

    $type = $types[array_rand($types)];
    $attrs = [
        'seller' => function () {
            return factory(User::class)->create()->id;
        },
        'price' => $faker->randomNumber(),
        'name' => $faker->sentence(),
        'description' => $faker->paragraph(),
        'type' => $type,
    ];

    if ($type === 'bomb') {
        $attrs['attributes'] = ['weight' => $faker->randomNumber()];
    } elseif ($type === 'accommodation') {
        $attrs['attributes'] = ['address' => $faker->sentence()];
    }

    return $attrs;
});

$factory->define(FightClub::class, function (Generator $faker) {
    return [
        'owner' => function () {
            return factory(User::class)->create()->id;
        },
        'name' => $faker->unique()->word,
        'membership_price' => $faker->randomNumber(),
    ];
});

$factory->define(Payment::class, function (Generator $faker) {
    static $types = ['fine', 'membership'];

    return [
        'from' => function () {
            return factory(User::class)->create()->id;
        },
        'to' => function () {
            return factory(User::class)->create()->id;
        },
        'amount' => $faker->randomNumber(),
        'paid' => $faker->boolean(),
        'type' => $types[array_rand($types)],
        'comment' => $faker->sentence(),
    ];
});

$factory->define(Meeting::class, function (Generator $faker) {
    return [
        'fight_club_id' => function () {
            return factory(FightClub::class)->create()->id;
        },
        'product_id' => function () {
            return factory(Product::class)->create()->id;
        },
        'at' => $faker->dateTime()->format('Y-m-d H:i:s'),
    ];
});
