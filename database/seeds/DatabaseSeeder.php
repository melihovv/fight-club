<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        Model::unguard();

        $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(FightClubsTableSeeder::class);
        $this->call(FightClubsMembersTableSeeder::class);
        $this->call(PaymentsTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(MeetingsTableSeeder::class);

        Model::reguard();
    }
}
