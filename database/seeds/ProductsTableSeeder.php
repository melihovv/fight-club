<?php

use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    public function run()
    {
        $usersIds = User::with(['roles' => function ($q) {
            $q->whereIn('name', ['accommodation-owner', 'bomb-seller']);
        }])->get()->pluck('id')->toArray();

        factory(Product::class, 100)
            ->create(['seller' => function () use ($usersIds) {
                return $usersIds[array_rand($usersIds)];
            }]);
    }
}
