<?php

use App\Models\FightClub;
use App\Models\Meeting;
use App\Models\Product;
use Illuminate\Database\Seeder;

class MeetingsTableSeeder extends Seeder
{
    public function run()
    {
        $fightClubsIds = FightClub::pluck('id')->toArray();
        $productsIds = Product::pluck('id')->toArray();

        factory(Meeting::class, 100)
            ->create([
                'fight_club_id' => function () use ($fightClubsIds) {
                    return $fightClubsIds[array_rand($fightClubsIds)];
                },
                'product_id' => function() use ($productsIds) {
                    return $productsIds[array_rand($productsIds)];
                },
            ]);
    }
}
