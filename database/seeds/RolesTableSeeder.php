<?php

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sell = factory(Permission::class)->create([
            'name' => 'sell',
            'display_name' => 'Sell',
            'description' => 'A man who can sell',
        ])->id;

        $buy = factory(Permission::class)->create([
            'name' => 'buy',
            'display_name' => 'Buy',
            'description' => 'A man who can buy',
        ])->id;

        $manageBalance = factory(Permission::class)->create([
            'name' => 'manage-balance',
            'display_name' => 'Manage balance',
            'description' => 'A man who can manage his balance',
        ])->id;

        $viewBalance = factory(Permission::class)->create([
            'name' => 'view-balance',
            'display_name' => 'View balance',
            'description' => 'A man who can view his balance',
        ])->id;

        $manageMeetings = factory(Permission::class)->create([
            'name' => 'manage-meetings',
            'display_name' => 'Manage meetings',
            'description' => 'A man who can manage meetings',
        ])->id;

        $manageMembers = factory(Permission::class)->create([
            'name' => 'manage-members',
            'display_name' => 'Manage members',
            'description' => 'A man who can manage fight clubs members',
        ])->id;

        $manageFightClub = factory(Permission::class)->create([
            'name' => 'manage-fight-club',
            'display_name' => 'Manage fight club',
            'description' => 'A man who can manage fight club',
        ])->id;

        $setGoals = factory(Permission::class)->create([
            'name' => 'set-goals',
            'display_name' => 'Set goals',
            'description' => 'A man who can set start balance and needed ' .
                'amount of bombs',
        ])->id;

        $beNotifiedAboutClubMeetings = factory(Permission::class)->create([
            'name' => 'be-notified-about-club-meetings',
            'display_name' => 'Be notified about club meetings',
            'description' => 'A man who can be notified about club meetings',
        ])->id;

        $buyMembership = factory(Permission::class)->create([
            'name' => 'buy-membership',
            'display_name' => 'Buy membership',
            'description' => 'A man who can buy fight club membership',
        ])->id;

        $payFines = factory(Permission::class)->create([
            'name' => 'pay-fines',
            'display_name' => 'Pay fines',
            'description' => 'A man who can pay fines',
        ])->id;

        $joinLeaveClub = factory(Permission::class)->create([
            'name' => 'join-leave-club',
            'display_name' => 'Join or leave club',
            'description' => 'A man who can join or leave fight club',
        ])->id;

        $fightClubsOwner = factory(Role::class)->create([
            'name' => 'fight-club-owner',
            'display_name' => 'Организатор бойцовского клуба',
            'description' => 'Организатор бойцовского клуба',
        ]);
        $fightClubsOwner->permissions()->attach([
            $manageBalance,
            $viewBalance,
            $buy,
            $manageMeetings,
            $manageMembers,
            $manageFightClub,
            $setGoals,
        ]);

        $fightClubsMember = factory(Role::class)->create([
            'name' => 'fight-clubs-member',
            'display_name' => 'Член бойцовских клубов',
            'description' => 'Член бойцовских клубов',
        ]);
        $fightClubsMember->permissions()->attach([
            $manageBalance,
            $viewBalance,
            $beNotifiedAboutClubMeetings,
            $buyMembership,
            $payFines,
            $joinLeaveClub,
        ]);

        $accommdationOwner = factory(Role::class)->create([
            'name' => 'accommodation-owner',
            'display_name' => 'Арендодатель помещений',
            'description' => 'Арендодатель помещений',
        ]);
        $accommdationOwner->permissions()->attach([
            $manageBalance,
            $viewBalance,
            $sell,
        ]);

        $bombSeller = factory(Role::class)->create([
            'name' => 'bomb-seller',
            'display_name' => 'Продавец бомб',
            'description' => 'Продавец бомб',
        ]);
        $bombSeller->permissions()->attach([
            $manageBalance,
            $viewBalance,
            $sell,
        ]);
    }
}
