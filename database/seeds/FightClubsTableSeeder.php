<?php

use App\Models\FightClub;
use App\Models\User;
use Illuminate\Database\Seeder;

class FightClubsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usersIds = User::all()->random(5)->pluck('id')->toArray();

        foreach (range(0, 4) as $i) {
            factory(FightClub::class)->create(['owner' => $usersIds[$i]]);
        }
    }
}
