<?php

use App\Models\Payment;
use App\Models\User;
use Illuminate\Database\Seeder;

class PaymentsTableSeeder extends Seeder
{
    public function run()
    {
        $usersIds = User::pluck('id')->toArray();

        foreach (range(1, 100) as $i) {
            do {
                $from = $usersIds[array_rand($usersIds)];
                $to = $usersIds[array_rand($usersIds)];
            } while ($from === $to);

            factory(Payment::class)->create(['from' => $from, 'to' => $to]);
        }
    }
}
