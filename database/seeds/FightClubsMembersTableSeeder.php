<?php

use App\Models\FightClub;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class FightClubsMembersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usersIds = User::all()->pluck('id');
        $fightClubsIds = FightClub::all()->pluck('id');

        $now = Carbon::now();

        foreach ($usersIds->random(5) as $userId) {
            foreach ($fightClubsIds->random(2) as $fightClubId) {
                DB::table('fight_clubs_members')->insert([
                    'fight_club_id' => $fightClubId,
                    'member' => $userId,
                    'created_at' => $now,
                    'updated_at' => $now,
                ]);
            }
        }
    }
}
