<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFightClubsMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fight_clubs_members', function (Blueprint $table) {
            $table->unsignedInteger('fight_club_id');
            $table->unsignedInteger('member');
            $table->timestamps();

            $table->foreign('fight_club_id')->references('id')
                ->on('fight_clubs')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('member')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->unique(['fight_club_id', 'member']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fight_clubs_members');
    }
}
