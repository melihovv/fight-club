<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaymentsTable extends Migration
{
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('from');
            $table->unsignedInteger('to');
            $table->unsignedInteger('amount');
            $table->enum('type', ['fine', 'membership']);
            $table->string('comment')->nullable();
            $table->boolean('paid')->default(false);
            $table->timestamps();

            $table->foreign('from')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('to')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('payments');
    }
}
