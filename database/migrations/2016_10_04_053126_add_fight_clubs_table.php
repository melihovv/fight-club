<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFightClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fight_clubs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('owner')->unique();
            $table->string('name')->unique();
            $table->double('membership_price');
            $table->timestamps();

            $table->foreign('owner')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fight_clubs');
    }
}
