<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMeetingsTable extends Migration
{
    public function up()
    {
        Schema::create('meetings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('fight_club_id');
            $table->unsignedInteger('product_id');
            $table->dateTime('at');
            $table->timestamps();

            $table->foreign('fight_club_id')->references('id')
                ->on('fight_clubs')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('product_id')->references('id')
                ->on('products')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('meetings');
    }
}
