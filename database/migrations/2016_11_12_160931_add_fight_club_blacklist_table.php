<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFightClubBlacklistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fight_club_blacklist', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('fight_club_id');
            $table->unsignedInteger('user_id');
            $table->timestamps();

            $table->foreign('fight_club_id')->references('id')
                ->on('fight_clubs')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->unique(['fight_club_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fight_club_blacklist');
    }
}
