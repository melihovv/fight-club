<?php

declare(strict_types=1);

abstract class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    use TestHelpers;

    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        Hash::setRounds(5);

        return $app;
    }
}
