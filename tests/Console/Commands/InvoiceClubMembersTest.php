<?php

declare(strict_types = 1);

use App\Models\FightClub;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class InvoiceClubMembersTest extends TestCase
{
    use DatabaseMigrations;

    public function testSuccess()
    {
        $owner1 = factory(User::class)->create();
        $owner2 = factory(User::class)->create();

        $created_at = Carbon::now()->subMonth();
        factory(User::class, 4)->create();

        $fightClub1 = factory(FightClub::class)->create([
            'owner' => $owner1->id,
            'membership_price' => 10,
        ]);
        $fightClub1->members()->attach([3, 4]);

        $fightClub2 = factory(FightClub::class)->create([
            'owner' => $owner2->id,
            'membership_price' => 20,
        ]);
        $fightClub2->members()->attach([5, 6]);

        DB::table('fight_clubs_members')
            ->whereIn('member', [3, 5])
            ->update(['created_at' => $created_at]);

        Artisan::call('invoice:members');

        $this->seeInDatabase('payments', [
            'amount' => 10,
            'from' => 3,
            'to' => $owner1->id,
            'type' => 'membership',
            'paid' => false,
        ]);

        $this->seeInDatabase('payments', [
            'amount' => 20,
            'from' => 5,
            'to' => $owner2->id,
            'type' => 'membership',
            'paid' => false,
        ]);
    }
}
