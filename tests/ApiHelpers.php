<?php

declare(strict_types=1);

use App\Models\User;

trait ApiHelpers
{
    protected $headersV1 = [
        'Accept' => 'application/x.fc.v1+json',
    ];

    /**
     * Return request headers needed to interact with the API.
     * @param string $version
     * @param User $user
     * @return array Array of headers.
     */
    protected function headers(string $version = 'v1', User $user = null)
    {
        if ($version === 'v1') {
            $headers = $this->headersV1;
        } else {
            $headers = [];
        }

        if (!is_null($user)) {
            $token = JWTAuth::fromUser($user);
            JWTAuth::setToken($token);
            $headers['Authorization'] = 'Bearer ' . $token;
        }

        return $headers;
    }
}
