<?php

declare(strict_types = 1);

use App\Models\User;
use Dingo\Api\Routing\UrlGenerator;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UsersControllerTest extends TestCase
{
    use DatabaseMigrations;
    use ApiHelpers;

    public function testGet()
    {
        $user = factory(User::class)->create();

        $route = app(UrlGenerator::class)->version('v1')
            ->route('users.get', [$user->id]);

        $this->get($route, $this->headers('v1', $user));

        $this->seeJsonStructure([
            'data' => [
                'id',
                'name',
                'email',
                'balance',
            ],
        ]);
    }
}
