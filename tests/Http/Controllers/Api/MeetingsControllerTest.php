<?php

declare(strict_types = 1);

use App\Models\FightClub;
use App\Models\Meeting;
use App\Models\Product;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Dingo\Api\Routing\UrlGenerator;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class MeetingsControllerTest extends TestCase
{
    use DatabaseMigrations;
    use ApiHelpers;

    public function testIndex()
    {
        $owner = factory(User::class)->create();
        $this->attachPermission($owner, 'manage-fight-club');

        $fightClub = factory(FightClub::class)->create(['owner' => $owner->id]);

        $seller = factory(User::class)->create();
        $this->attachPermission($seller, 'sell');
        $product = factory(Product::class)->create([
            'seller' => $seller->id,
            'type' => 'accommodation',
            'attributes' => [
                'address' => 'addr',
            ],
        ]);

        factory(Meeting::class, 10)->create([
            'fight_club_id' => $fightClub->id,
            'product_id' => $product->id,
        ]);

        $route = app(UrlGenerator::class)->version('v1')
            ->route('meetings.index');

        $this->get($route, $this->headers('v1', $owner));

        $this->seeJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'fight_club_id',
                    'product_id',
                    'address',
                    'at',
                ],
            ],
        ]);
    }

    public function testDestory()
    {
        $owner = factory(User::class)->create();
        $this->attachPermission($owner, 'manage-fight-club');

        $fightClub = factory(FightClub::class)->create(['owner' => $owner->id]);

        $seller = factory(User::class)->create();
        $this->attachPermission($seller, 'sell');
        $product = factory(Product::class)->create([
            'seller' => $seller->id,
            'type' => 'accommodation',
            'attributes' => [
                'address' => 'addr',
            ],
        ]);

        factory(Meeting::class, 10)->create([
            'fight_club_id' => $fightClub->id,
            'product_id' => $product->id,
        ]);

        $route = app(UrlGenerator::class)->version('v1')
            ->route('meetings.destroy', [1]);

        $this->delete($route, [], $this->headers('v1', $owner));

        $this->assertResponseStatus(204);
    }

    public function testMember()
    {
        $member = factory(User::class)->create();
        $this->attachRole($member, 'fight-clubs-member');

        $owner1 = factory(User::class)->create();
        $this->attachPermission($owner1, 'manage-fight-club');
        $fightClub1 = factory(FightClub::class)
            ->create(['owner' => $owner1->id]);
        $fightClub1->members()->attach([$member->id]);

        $owner2 = factory(User::class)->create();
        $role = factory(Role::class)->create();
        $owner2->roles()->attach($role->id);
        $owner2->roles()->first()->permissions()->attach(1);
        $fightClub2 = factory(FightClub::class)
            ->create(['owner' => $owner2->id]);
        $fightClub2->members()->attach([$member->id]);

        $seller = factory(User::class)->create();
        $this->attachPermission($seller, 'sell');

        $product1 = factory(Product::class)->create([
            'seller' => $seller->id,
            'type' => 'accommodation',
            'attributes' => [
                'address' => 'addr1',
            ],
        ]);

        $product2 = factory(Product::class)->create([
            'seller' => $seller->id,
            'type' => 'accommodation',
            'attributes' => [
                'address' => 'addr2',
            ],
        ]);

        factory(Meeting::class, 10)->create([
            'fight_club_id' => $fightClub1->id,
            'product_id' => $product1->id,
            'at' => Carbon::now()->addDay(),
        ]);

        factory(Meeting::class, 10)->create([
            'fight_club_id' => $fightClub2->id,
            'product_id' => $product2->id,
            'at' => Carbon::now()->addDay(),
        ]);

        $route = app(UrlGenerator::class)->version('v1')
            ->route('meetings.member');

        $this->get($route, $this->headers('v1', $member));

        $this->seeJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'fight_club_id',
                    'product_id',
                    'address',
                    'at',
                ],
            ],
        ]);
    }

    public function testAdd()
    {
        $owner = factory(User::class)->create([
            'balance' => 1000,
        ]);
        $this->attachPermission($owner, 'manage-fight-club');

        factory(FightClub::class)->create(['owner' => $owner->id]);

        $seller = factory(User::class)->create([
            'balance' => 1000,
        ]);
        $this->attachPermission($seller, 'sell');
        $product = factory(Product::class)->create([
            'price' => 100,
            'seller' => $seller->id,
            'type' => 'accommodation',
            'attributes' => [
                'address' => 'addr',
            ],
        ]);

        $route = app(UrlGenerator::class)->version('v1')
            ->route('meetings.add');

        $this->post($route, [
            'product_id' => $product->id,
            'at' => '2016-12-17 00:00:00',
        ], $this->headers('v1', $owner));

        $this->assertResponseStatus(204);

        $this->seeInDatabase('users', [
            'id' => 1,
            'balance' => 900,
        ]);

        $this->seeInDatabase('users', [
            'id' => 2,
            'balance' => 1100,
        ]);

        $this->seeInDatabase('meetings', [
            'fight_club_id' => 1,
            'product_id' => 1,
            'at' => '2016-12-17 00:00:00',
        ]);
    }
}
