<?php

declare(strict_types=1);

use App\Models\User;
use Dingo\Api\Routing\UrlGenerator;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class BalanceControllerTest extends TestCase
{
    use DatabaseMigrations;
    use ApiHelpers;

    public function testGet()
    {
        $user = factory(User::class)->create([
            'balance' => 1000,
        ]);
        $this->attachPermission($user, 'view-balance');

        $route = app(UrlGenerator::class)->version('v1')->route('balance.get');

        $this->get($route, $this->headers('v1', $user));

        $this->seeJsonEquals([
            'balance' => 1000,
        ]);
    }

    public function testFillUp()
    {
        $user = factory(User::class)->create([
            'balance' => 1000,
        ]);
        $this->attachPermission($user, 'manage-balance');

        $route = app(UrlGenerator::class)->version('v1')
            ->route('balance.fill-up');

        $this->post($route, [
            'amount' => 100,
        ], $this->headers('v1', $user));

        $this->assertResponseStatus(204);
        $this->seeInDatabase('users', [
            'balance' => 1100,
        ]);
    }
}
