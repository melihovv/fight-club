<?php

declare(strict_types = 1);

use App\Models\FightClub;
use App\Models\User;
use Dingo\Api\Routing\UrlGenerator;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class FightClubsControllerTest extends TestCase
{
    use DatabaseMigrations;
    use ApiHelpers;

    public function testList()
    {
        $user = factory(User::class)->create();
        factory(FightClub::class)->create()->members()->attach([$user->id]);

        $fightClub = factory(FightClub::class)->create([
            'owner' => $user->id,
            'name' => 'n',
            'membership_price' => 10,
        ]);
        $member1 = factory(User::class)->create();
        $member2 = factory(User::class)->create();
        $fightClub->members()->attach([$member1->id, $member2->id]);

        $route = app(UrlGenerator::class)->version('v1')->route('fight-clubs');

        $this->get($route, $this->headers('v1', $user));

        $this->seeJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'owner',
                    'name',
                    'membership_price',
                    'members_count',
                ],
            ],
            'meta' => [
                'joined_fight_clubs' => [],
            ],
        ]);
    }

    public function testJoin()
    {
        $user = factory(User::class)->create();
        $this->attachPermission($user, 'join-leave-club');
        $fightClub = factory(FightClub::class)->create();

        $route = app(UrlGenerator::class)->version('v1')
            ->route('fight-clubs.join', [$fightClub->id]);

        $this->post($route, [], $this->headers('v1', $user));

        $this->assertResponseStatus(204);

        $this->seeInDatabase('fight_clubs_members', [
            'fight_club_id' => $fightClub->id,
            'member' => $user->id,
        ]);
    }

    public function testJoinUserInBlacklist()
    {
        $user = factory(User::class)->create();
        $this->attachPermission($user, 'join-leave-club');

        $fightClub = factory(FightClub::class)->create();
        $fightClub->blacklistedUsers()->attach($user->id);

        $route = app(UrlGenerator::class)->version('v1')
            ->route('fight-clubs.join', [$fightClub->id]);

        $this->post($route, [], $this->headers('v1', $user));

        $this->assertResponseStatus(400);
    }

    public function testLeave()
    {
        $user = factory(User::class)->create();
        $this->attachPermission($user, 'join-leave-club');
        $fightClub = factory(FightClub::class)->create();
        $fightClub->members()->attach([$user->id]);

        $route = app(UrlGenerator::class)->version('v1')
            ->route('fight-clubs.leave', [$fightClub->id]);

        $this->post($route, [], $this->headers('v1', $user));

        $this->assertResponseStatus(204);

        $this->assertNotContains(
            $user->id,
            $fightClub->members()->pluck('id')->toArray()
        );
    }
}
