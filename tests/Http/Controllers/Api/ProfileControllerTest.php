<?php

declare(strict_types = 1);

use App\Models\User;
use Dingo\Api\Routing\UrlGenerator;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ProfileControllerTest extends TestCase
{
    use DatabaseMigrations;
    use ApiHelpers;

    public function testUpdate()
    {
        $user = factory(User::class)->create();

        $route = app(UrlGenerator::class)->version('v1')
            ->route('profile.update');

        $this->put($route, [
            'name' => 'newnick',
            'email' => 'newnick@ya.ru',
            'old_password' => 'secret',
            'password' => 'secret2',
        ], $this->headers('v1', $user));

        $this->assertResponseStatus(204);

        $this->seeInDatabase('users', [
            'name' => 'newnick',
            'email' => 'newnick@ya.ru',
        ]);

        $this->assertTrue(Hash::check('secret2', User::first()->password));
    }

    public function testUpdateFightClubOwner()
    {
        $user = factory(User::class)->create();
        $this->attachRole($user, 'fight-club-owner');

        $route = app(UrlGenerator::class)->version('v1')
            ->route('profile.update');

        $this->put($route, [
            'name' => 'newnick',
            'email' => 'newnick@ya.ru',
            'old_password' => 'secret',
            'password' => 'secret2',
            'goal' => 1111,
        ], $this->headers('v1', $user));

        $this->assertResponseStatus(204);

        $this->seeInDatabase('users', [
            'name' => 'newnick',
            'email' => 'newnick@ya.ru',
        ]);

        $newUser = User::first();
        $this->assertTrue(Hash::check('secret2', $newUser->password));
        $this->assertEquals(1111, $newUser->attributes['goal']);
    }
}
