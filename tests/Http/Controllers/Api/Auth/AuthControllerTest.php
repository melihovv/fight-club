<?php

declare(strict_types=1);

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Dingo\Api\Routing\UrlGenerator;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AuthControllerTest extends DatabaseTestCase
{
    use DatabaseMigrations;
    use ApiHelpers;

    public function testRegister()
    {
        $role = factory(Role::class)->create([
            'name' => 'fight-clubs-member',
        ]);

        factory(Permission::class)->create()->roles()->attach([1]);

        $route = app(UrlGenerator::class)->version('v1')->route('register');

        $this->post($route, [
            'name' => 'name',
            'email' => 'a@ya.ru',
            'password' => 'secret',
            'balance' => 100,
            'role' => $role->id,
        ], $this->headersV1);

        $this->seeJsonStructure([
            'data' => [
                'id',
                'name',
                'email',
                'balance',
                'roles' => $this->rolesJsonStructure(),
            ],
            'meta' => [
                'token',
            ],
        ]);

        $this->seeInDatabase('users', [
            'name' => 'name',
            'email' => 'a@ya.ru',
            'balance' => 100,
        ]);

        $this->assertTrue(User::first()->hasRole('fight-clubs-member'));
    }

    public function testRegisterFightClubsOwner()
    {
        $role = factory(Role::class)->create([
            'name' => 'fight-club-owner',
        ]);

        factory(Permission::class)->create()->roles()->attach([1]);

        $route = app(UrlGenerator::class)->version('v1')->route('register');

        $this->post($route, [
            'name' => 'name',
            'email' => 'a@ya.ru',
            'password' => 'secret',
            'balance' => 1000,
            'role' => $role->id,
            'fight_club_name' => 'taylor club',
            'membership_price' => 100,
            'goal' => 10000,
        ], $this->headersV1);

        $this->seeJsonStructure([
            'data' => [
                'id',
                'name',
                'email',
                'balance',
                'attributes' => [
                    'goal',
                ],
                'roles' => $this->rolesJsonStructure(),
            ],
            'meta' => [
                'token',
            ],
        ]);

        $this->seeInDatabase('users', [
            'name' => 'name',
            'email' => 'a@ya.ru',
            'balance' => 1000,
        ]);
        $this->assertEquals(User::first()->attributes['goal'], 10000);

        $this->seeInDatabase('fight_clubs', [
            'name' => 'taylor club',
            'membership_price' => 100,
        ]);

        $this->assertTrue(User::first()->hasRole('fight-club-owner'));
    }

    public function testLogin()
    {
        $user = factory(User::class)->create([
            'email' => 'a@ya.ru',
        ]);

        $this->attachRolesAndPermissionsToUser($user);

        $route = app(UrlGenerator::class)->version('v1')->route('login');

        $this->post($route, [
            'email' => 'a@ya.ru',
            'password' => 'secret',
        ], $this->headers());

        $this->seeJsonStructure([
            'data' => [
                'id',
                'name',
                'email',
                'balance',
                'roles' => $this->rolesJsonStructure(),
            ],
            'meta' => [
                'token',
            ],
        ]);
    }

    public function testUser()
    {
        $user = factory(User::class)->create();

        $this->attachRolesAndPermissionsToUser($user);

        $route = app(UrlGenerator::class)->version('v1')->route('user');

        $this->get($route, $this->headers('v1', $user));

        $this->seeJsonStructure([
            'data' => [
                'id',
                'name',
                'email',
                'balance',
                'roles' => $this->rolesJsonStructure(),
            ],
        ]);
    }

    protected function rolesJsonStructure() : array
    {
        return [
            'data' => [
                '*' => [
                    'id',
                    'name',
                    'display_name',
                    'description',
                    'permissions' => [
                        'data' => [
                            '*' => [
                                'id',
                                'name',
                                'display_name',
                                'description',
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    protected function attachRolesAndPermissionsToUser(User $user)
    {
        factory(Role::class, 2)->create();
        $user->roles()->attach([1, 2]);

        $permission1 = factory(Permission::class)->create();
        $permission1->roles()->attach([1, 2]);

        $permission2 = factory(Permission::class)->create();
        $permission2->roles()->attach([1, 2]);
    }
}
