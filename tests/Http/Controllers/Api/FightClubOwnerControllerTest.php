<?php

declare(strict_types = 1);

use App\Models\FightClub;
use App\Models\User;
use Dingo\Api\Routing\UrlGenerator;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class FightClubOwnerControllerTest extends TestCase
{
    use DatabaseMigrations;
    use ApiHelpers;

    public function testGet()
    {
        $user = factory(User::class)->create();
        $this->attachPermission($user, 'manage-fight-club');

        factory(User::class, 3)->create();
        $fightClub = factory(FightClub::class)->create([
            'owner' => $user->id,
            'name' => 'n',
            'membership_price' => 10,
        ]);

        $fightClub->members()->attach([2, 3]);
        $fightClub->blacklistedUsers()->attach([4]);

        $route = app(UrlGenerator::class)->version('v1')
            ->route('fight-club.get');

        $this->get($route, $this->headers('v1', $user));

        $this->seeJsonStructure([
            'data' => [
                'id',
                'owner',
                'name',
                'membership_price',
                'members' => [
                    'data' => [
                        '*' => [
                            'id',
                            'name',
                            'email',
                            'balance',
                        ],
                    ],
                ],
                'banned_users' => [
                    'data' => [
                        '*' => [
                            'id',
                            'name',
                            'email',
                            'balance',
                        ],
                    ],
                ],
            ],
        ]);
    }

    public function testUpdate()
    {
        $user = factory(User::class)->create();
        $this->attachPermission($user, 'manage-fight-club');
        factory(FightClub::class)->create([
            'owner' => $user->id,
            'name' => 'n',
            'membership_price' => 10,
        ]);

        $route = app(UrlGenerator::class)->version('v1')
            ->route('fight-club.update');

        $this->put($route, [
            'name' => 'name',
            'membership_price' => 20,
            'owner' => 100,
        ], $this->headers('v1', $user));

        $this->assertResponseStatus(204);
        $this->seeInDatabase('fight_clubs', [
            'name' => 'name',
            'membership_price' => 20,
            'owner' => $user->id,
        ]);
    }

    public function testExclude()
    {
        $user = factory(User::class)->create();
        $this->attachPermission($user, 'manage-fight-club');

        $fightClub = factory(FightClub::class)->create(['owner' => $user->id]);
        $member = factory(User::class)->create();
        $fightClub->members()->attach($member->id);

        $route = app(UrlGenerator::class)->version('v1')
            ->route('fight-club.exclude', [$member->id]);

        $this->post($route, [], $this->headers('v1', $user));

        $this->assertResponseStatus(204);

        $this->assertContains(
            $member->id,
            $fightClub->blacklistedUsers->pluck('id')->toArray()
        );

        $this->assertNotContains(
            $member->id,
            $fightClub->members()->pluck('id')->toArray()
        );
    }

    public function testExcludeFromBlacklist()
    {
        $user = factory(User::class)->create();
        $this->attachPermission($user, 'manage-fight-club');

        $fightClub = factory(FightClub::class)->create(['owner' => $user->id]);
        $member = factory(User::class)->create();
        $fightClub->blacklistedUsers()->attach($member->id);

        $route = app(UrlGenerator::class)->version('v1')
            ->route('fight-club.exclude-from-blacklist', [$member->id]);

        $this->post($route, [], $this->headers('v1', $user));

        $this->assertResponseStatus(204);

        $this->assertNotContains(
            $member->id,
            $fightClub->blacklistedUsers->pluck('id')->toArray()
        );
    }

    public function testFine()
    {
        $user = factory(User::class)->create();
        $this->attachPermission($user, 'manage-fight-club');

        $fightClub = factory(FightClub::class)->create(['owner' => $user->id]);
        $member = factory(User::class)->create();
        $fightClub->members()->attach($member->id);

        $route = app(UrlGenerator::class)->version('v1')
            ->route('fight-club.fine', [$member->id]);

        $this->post($route, [
            'amount' => 10,
        ], $this->headers('v1', $user));

        $this->assertResponseStatus(204);

        $this->assertEquals(10, $member->myPayments()->first()->amount);
    }

    public function testFineUserWhichIsNotOurFightClub()
    {
        $user = factory(User::class)->create();
        $this->attachPermission($user, 'manage-fight-club');
        factory(FightClub::class)->create(['owner' => $user->id]);

        $member = factory(User::class)->create();

        $route = app(UrlGenerator::class)->version('v1')
            ->route('fight-club.fine', [$member->id]);

        $this->post($route, [
            'amount' => 10,
        ], $this->headers('v1', $user));

        $this->assertResponseStatus(400);

        $this->assertCount(0, $member->myPayments);
    }
}
