<?php

declare(strict_types = 1);

use App\Models\Role;
use Dingo\Api\Routing\UrlGenerator;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RolesControllerTest extends TestCase
{
    use DatabaseMigrations;
    use ApiHelpers;

    public function testIndex()
    {
        factory(Role::class, 5)->create();

        $route = app(UrlGenerator::class)->version('v1')->route('roles.index');

        $this->get($route, $this->headersV1)
            ->seeJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'name',
                        'display_name',
                        'description',
                    ],
                ],
            ]);
    }
}
