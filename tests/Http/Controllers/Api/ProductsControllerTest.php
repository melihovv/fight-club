<?php

declare(strict_types = 1);

use App\Models\Product;
use App\Models\User;
use Dingo\Api\Routing\UrlGenerator;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ProductsControllerTest extends TestCase
{
    use DatabaseMigrations;
    use ApiHelpers;

    public function testIndex()
    {
        $user = factory(User::class)->create();
        $this->attachPermission($user, 'buy');

        factory(Product::class, 10)->create(['type' => 'bomb']);

        $route = app(UrlGenerator::class)->version('v1')
            ->route('products.index', ['type' => 'bomb']);

        $this->get($route, $this->headers('v1', $user));

        $this->seeJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'name',
                    'description',
                    'seller',
                    'price',
                    'type',
                    'attributes',
                ],
            ],
        ]);
    }

    public function testBuy()
    {
        $user = factory(User::class)->create([
            'balance' => 1000,
            'attributes' => [
                'goal' => 1000,
            ],
        ]);
        $this->attachPermission($user, 'buy');

        $seller = factory(User::class)->create([
            'balance' => 1000,
        ]);
        $this->attachRole($seller, 'bomb-seller');

        factory(Product::class, 10)->create([
            'price' => 100,
            'seller' => $seller->id,
            'type' => 'bomb',
            'attributes' => [
                'weight' => 10,
            ],
        ]);

        $route = app(UrlGenerator::class)->version('v1')
            ->route('products.buy', [1]);

        $this->post($route, [], $this->headers('v1', $user));

        $this->assertResponseStatus(204);

        $this->seeInDatabase('users', [
            'id' => 1,
            'balance' => 900,
        ]);
        $this->seeInDatabase('users', [
            'id' => 2,
            'balance' => 1100,
        ]);

        $this->assertEquals([
            'goal' => 1000,
            'bombs_weight' => 10,
        ], User::first()->attributes);
    }

    public function testBuyFailure()
    {
        $user = factory(User::class)->create(['balance' => 1000]);
        $this->attachPermission($user, 'buy');

        $seller = factory(User::class)->create();
        $this->attachRole($seller, 'bomb-seller');

        factory(Product::class, 10)->create([
            'price' => 9999,
        ]);

        $route = app(UrlGenerator::class)->version('v1')
            ->route('products.buy', [1]);

        $this->post($route, [], $this->headers('v1', $user));

        $this->assertResponseStatus(400);
    }

    public function testMy()
    {
        $user = factory(User::class)->create();
        $this->attachPermission($user, 'buy');

        factory(Product::class, 10)->create([
            'type' => 'bomb',
            'seller' => $user->id,
        ]);

        $route = app(UrlGenerator::class)->version('v1')
            ->route('products.my', ['type' => 'bomb']);

        $this->get($route, $this->headers('v1', $user));

        $this->seeJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'name',
                    'description',
                    'seller',
                    'price',
                    'type',
                    'attributes',
                ],
            ],
        ]);
    }

    public function testAdd()
    {
        $user = factory(User::class)->create();
        $this->attachPermission($user, 'sell');
        $this->attachRole($user, 'bomb-seller');

        $route = app(UrlGenerator::class)->version('v1')->route('products.add');

        $this->post($route, [
            'seller' => 111,
            'price' => 100,
            'name' => 'product',
            'type' => 'bomb',
            'weight' => 10,
        ], $this->headers('v1', $user));

        $this->assertResponseStatus(204);

        $this->seeInDatabase('products', [
            'seller' => $user->id,
            'price' => 100,
            'name' => 'product',
            'type' => 'bomb',
        ]);
        $this->assertEquals(10, Product::first()->attributes['weight']);
    }

    public function testGet()
    {
        $user = factory(User::class)->create();
        $this->attachPermission($user, 'sell');

        factory(Product::class, 10)->create();

        $route = app(UrlGenerator::class)->version('v1')
            ->route('products.get', [1]);

        $this->get($route, $this->headers('v1', $user));

        $this->seeJsonStructure([
            'data' => [
                'seller',
                'price',
                'name',
                'type',
                'attributes',
            ],
        ]);
    }

    public function testDestroy()
    {
        $user = factory(User::class)->create();
        $this->attachPermission($user, 'sell');

        factory(Product::class, 10)->create();

        $route = app(UrlGenerator::class)->version('v1')
            ->route('products.destroy', [1]);

        $this->delete($route, [], $this->headers('v1', $user));

        $this->assertResponseStatus(204);
        $this->assertNull(Product::find(1));
    }

    public function testUpdate()
    {
        $user = factory(User::class)->create();
        $this->attachPermission($user, 'sell');

        factory(Product::class)->create([
            'price' => 100,
            'name' => 'product',
            'type' => 'accommodation',
        ]);

        $route = app(UrlGenerator::class)->version('v1')
            ->route('products.update', [1]);

        $this->put($route, [
            'price' => 111,
            'name' => 'product2',
            'type' => 'accommodation',
            'address' => 'addr',
        ], $this->headers('v1', $user));

        $this->assertResponseStatus(204);

        $this->seeInDatabase('products', [
            'price' => 111,
            'name' => 'product2',
        ]);
        $this->assertEquals('addr', Product::first()->attributes['address']);
    }
}
