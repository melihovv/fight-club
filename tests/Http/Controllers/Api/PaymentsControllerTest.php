<?php

declare(strict_types = 1);

use App\Models\FightClub;
use App\Models\Payment;
use App\Models\User;
use Dingo\Api\Routing\UrlGenerator;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class PaymentsControllerTest extends TestCase
{
    use DatabaseMigrations;
    use ApiHelpers;

    public function testGet()
    {
        $member = factory(User::class)->create();
        $this->attachRole($member, 'fight-clubs-member');

        $owner = factory(User::class)->create();
        $fightClub = factory(FightClub::class)->create(['owner' => $owner->id]);
        $fightClub->members()->attach([$member->id]);

        factory(Payment::class)->create([
            'from' => $member->id,
            'to' => $owner->id,
            'type' => 'membership',
            'amount' => 10,
            'comment' => 'com',
            'paid' => false,
        ]);

        $route = app(UrlGenerator::class)->version('v1')
            ->route('payments.get');

        $this->get($route, $this->headers('v1', $member));

        $this->seeJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'from',
                    'to',
                    'amount',
                    'comment',
                    'type',
                    'paid',
                ],
            ],
        ]);
    }

    public function testPay()
    {
        $member = factory(User::class)->create(['balance' => 20]);
        $this->attachRole($member, 'fight-clubs-member');

        $owner = factory(User::class)->create(['balance' => 100]);
        $fightClub = factory(FightClub::class)->create(['owner' => $owner->id]);
        $fightClub->members()->attach([$member->id]);

        $payment = factory(Payment::class)->create([
            'from' => $member->id,
            'to' => $owner->id,
            'type' => 'membership',
            'amount' => 10,
            'comment' => 'com',
            'paid' => false,
        ]);

        $route = app(UrlGenerator::class)->version('v1')
            ->route('payments.pay', $payment->id);

        $this->post($route, [], $this->headers('v1', $member));

        $this->assertResponseStatus(204);

        $this->seeInDatabase('users', [
            'id' => 1,
            'balance' => 10,
        ]);

        $this->seeInDatabase('users', [
            'id' => 2,
            'balance' => 110,
        ]);

        $this->assertTrue(Payment::first()->paid);
    }
}
