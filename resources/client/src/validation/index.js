import Vue from 'vue'
import VeeValidate, {Validator} from 'vee-validate'

import messages from './locale/ru'
import ValidationHelper from './validation-helper'

Validator.updateDictionary({
  ru: {
    messages,
    attributes: {
      membership_price: 'членский взнос',
      balance: 'баланс',
      role: 'роль',
      goal: 'цель',
      password: 'пароль',
      description: 'описание',
    },
  },
})

Vue.use(VeeValidate, {locale: 'ru'})
Vue.use(ValidationHelper)
