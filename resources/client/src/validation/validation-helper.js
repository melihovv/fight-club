export default {
  install: function (Vue, options) {
    Vue.prototype.$getValidationErrorsFromServer = getValidationErrorsFromServer
  },
}

function getValidationErrorsFromServer(options) {
  if (options.response && options.response.status === 422) {
    const errors = options.response.body.errors

    for (const field in errors) {
      if (errors.hasOwnProperty(field)) {
        const prefix = options.prefix
        this.errors.add(prefix ? prefix + field : field, errors[field][0])
      }
    }
  }
}

export {getValidationErrorsFromServer}
