import {PUSH_MESSAGE} from 'src/vuex/action-types'
import store from 'src/vuex/store'

export default {
  install: function (Vue, options) {
    Vue.prototype.$pushErrorMessage = pushErrorMessage
  },
}

function pushErrorMessage(options) {
  const response = options.response

  if (response
    && ![401, 422].includes(response.status)
    && !response.status.toString().startsWith('2')
  ) {
    store.dispatch(PUSH_MESSAGE, {
      msg: response.body.message,
      type: 'danger',
    })
  }
}

export {pushErrorMessage}
