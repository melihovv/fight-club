/* eslint-env browser */

import Vue from 'vue'
import Resource from 'vue-resource'
import store from 'src/vuex/store'
import {TOKEN_EXPIRED} from 'src/vuex/action-types'
import {pushErrorMessage} from 'src/helper'

Vue.use(Resource)

Vue.http.headers.common['Accept'] = 'application/x.fc.v1+json'

Vue.http.interceptors.push((request, next) => {
  const user = JSON.parse(localStorage.getItem('user'))

  if (user) {
    request.headers.set('Authorization', `Bearer ${user.token}`)
  }

  next(response => {
    if (response.status === 401) {
      const user = store.state.user

      if (user && user.loggedIn) {
        store.dispatch(TOKEN_EXPIRED)
      }
    } else {
      pushErrorMessage({response})
    }
  })
})

export const Role = Vue.resource('api/roles{/id}')
