import Vue from 'vue'
import Router from 'vue-router'
import store from './vuex/store'
import Register from 'components/Auth/Register'
import Login from 'components/Auth/Login'
import Dashboard from 'components/Dashboard'
import BalanceFillUp from 'components/Balance/FillUp'
import FightClubShow from 'components/FightClub/Show'
import FightClubEdit from 'components/FightClub/Edit'
import FightClubFine from 'components/FightClub/Fine'
import FightClubList from 'components/FightClub/List'
import MemberPayments from 'components/Member/Payments'
import Profile from 'components/Profile/Profile'
import ProfileEdit from 'components/Profile/Edit'
import ProductsIndex from 'components/Products/Index'
import ProductsCreate from 'components/Products/Create'
import ProductsEdit from 'components/Products/Edit'
import Bombs from 'components/Owner/Bombs'
import Meetings from 'components/Meetings/Meetings'
import MeetingsCreate from 'components/Meetings/Create'
import MemberMeetings from 'components/Member/Meetings'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  linkActiveClass: 'active',
  scrollBehavior: () => ({y: 0}),
  routes: [
    {
      path: '/register',
      name: 'register',
      component: Register,
      meta: {
        guest: true,
      },
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        guest: true,
      },
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/balance/fill-up',
      name: 'balance.fill-up',
      component: BalanceFillUp,
      meta: {
        requiresAuth: true,
        perm: 'manage-balance',
      },
    },
    {
      path: '/fight-club',
      name: 'fight-club.show',
      component: FightClubShow,
      meta: {
        requiresAuth: true,
        perm: 'manage-fight-club',
      },
    },
    {
      path: '/fight-club/edit',
      name: 'fight-club.edit',
      component: FightClubEdit,
      meta: {
        requiresAuth: true,
        perm: 'manage-fight-club',
      },
    },
    {
      path: '/fight-club/fine/:user',
      name: 'fight-club.fine',
      component: FightClubFine,
      meta: {
        requiresAuth: true,
        perm: 'manage-fight-club',
      },
    },
    {
      path: '/fight-clubs',
      name: 'fight-clubs',
      component: FightClubList,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/member/payments',
      name: 'member.payments',
      component: MemberPayments,
      meta: {
        requiresAuth: true,
        role: 'fight-clubs-member',
      },
    },
    {
      path: '/member/meetings',
      name: 'member.meetings',
      component: MemberMeetings,
      meta: {
        requiresAuth: true,
        role: 'fight-clubs-member',
      },
    },
    {
      path: '/profile/edit',
      name: 'profile.edit',
      component: ProfileEdit,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/products',
      name: 'products.index',
      component: ProductsIndex,
      meta: {
        requiresAuth: true,
        perm: 'sell',
      },
    },
    {
      path: '/products/create',
      name: 'products.create',
      component: ProductsCreate,
      meta: {
        requiresAuth: true,
        perm: 'sell',
      },
    },
    {
      path: '/products/edit/:productId',
      name: 'products.edit',
      component: ProductsEdit,
      meta: {
        requiresAuth: true,
        perm: 'sell',
      },
    },
    {
      path: '/buy-bombs',
      name: 'owner.buy-bombs',
      component: Bombs,
      meta: {
        requiresAuth: true,
        perm: 'buy',
      },
    },
    {
      path: '/meetings',
      name: 'meetings.index',
      component: Meetings,
      meta: {
        requiresAuth: true,
        perm: 'manage-fight-club',
      },
    },
    {
      path: '/meetings/create',
      name: 'meetings.create',
      component: MeetingsCreate,
      meta: {
        requiresAuth: true,
        perm: 'manage-fight-club',
      },
    },
    {
      path: '*',
      redirect: store.state.user.loggedIn ? '/dashboard' : '/login',
    },
  ],
})

router.beforeEach((to, from, next) => {
  let perm = null
  const permCheckRequired = to.matched.some(record => {
    perm = record.meta.perm
    return perm
  })

  if (permCheckRequired) {
    if (!store.getters.can(perm)) {
      return next({name: 'dashboard'})
    }
  }

  let role = null
  const roleCheckRequired = to.matched.some(record => {
    role = record.meta.role
    return role
  })

  if (roleCheckRequired) {
    if (!store.getters.hasRole(role)) {
      return next({name: 'dashboard'})
    }
  }

  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.state.user.loggedIn) {
      return next({name: 'login'})
    }
  }

  if (to.matched.some(record => record.meta.guest)) {
    if (store.state.user.loggedIn) {
      return next({name: 'dashboard'})
    } else {
      return next()
    }
  }

  return next()
})

export default router
