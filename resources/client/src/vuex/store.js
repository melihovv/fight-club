/* eslint-env browser */
import Vue from 'vue'
import Vuex from 'vuex'
import router from 'src/routes'
import 'src/resource'
import {getValidationErrorsFromServer} from 'src/validation/validation-helper'
import {pushErrorMessage} from 'src/helper'
import {
  SET_USER,
  CLEAR_USER,
  PUSH_MESSAGE,
  REMOVE_MESSAGE_BY_ID,
  CLEAR_MESSAGES,
} from './mutation-types'
import {
  LOGIN,
  LOGOUT,
  REGISTER,
  PUSH_MESSAGE as ACTION_PUSH_MESSAGE,
  TOKEN_EXPIRED,
} from './action-types'

Vue.use(Vuex)

const store = new Vuex.Store({
  strict: process.env.NODE_ENV === 'development',
  state: {
    user: {
      loggedIn: false,
      id: null,
      email: null,
      name: null,
      balance: 0,
      roles: [],
      permissions: [],
      token: null,
    },
    messages: [],
  },
  mutations: {
    [SET_USER](state, {
      id,
      email,
      name,
      balance,
      roles,
      permissions,
      loggedIn,
      token,
    }) {
      state.user.id = id || state.user.id
      state.user.email = email || state.user.email
      state.user.name = name || state.user.name
      state.user.balance = balance || state.user.balance
      state.user.token = token || state.user.token
      state.user.loggedIn = loggedIn || state.user.loggedIn

      if (roles) {
        state.user.roles = []
        state.user.permissions = []

        for (const role of roles.data ? roles.data : roles) {
          if (role.permissions) {
            state.user.permissions.push(
              ...(
                role.permissions.data
                  ? role.permissions.data
                  : role.permissions
              )
            )
          } else if (permissions) {
            state.user.permissions = permissions
          }

          delete role.permissions
          state.user.roles.push(role)
        }
      }

      localStorage.setItem('user', JSON.stringify(state.user))
    },
    [CLEAR_USER](state) {
      state.user = {
        loggedIn: false,
        id: null,
        email: null,
        name: null,
        balance: 0,
        roles: [],
        permissions: [],
        token: null,
      }

      localStorage.removeItem('user')
    },
    [PUSH_MESSAGE](state, {msg, type, id = null}) {
      if (id === null) {
        id = getNextMessageId(state)
      }

      state.messages.push({id, msg, type})
    },
    [REMOVE_MESSAGE_BY_ID](state, id) {
      let index = null
      state.messages.forEach((message, i) => {
        if (message.id === id) {
          index = i
        }
      })

      if (index !== null && index >= 0) {
        state.messages.splice(index, 1)
      }
    },
    [CLEAR_MESSAGES](state) {
      state.messages = []
    },
  },
  actions: {
    [REGISTER]({commit}, {
      ctx,
      name,
      email,
      password,
      fightClubName,
      balance,
      goal,
      membershipPrice,
      role,
      redirect = false,
    }) {
      Vue.http.post('/api/register', {
        name,
        email,
        password,
        fight_club_name: fightClubName,
        balance: balance,
        goal,
        membership_price: membershipPrice,
        role,
      })
        .then(response => {
          commit(SET_USER, {
            ...response.body.data,
            token: response.body.meta.token,
            loggedIn: true,
          })

          if (redirect) {
            router.push({name: redirect})
          }
        }, response => {
          getValidationErrorsFromServer.call(ctx, {response})
          pushErrorMessage.call(ctx, {response})
        })
    },
    [LOGIN]({commit, dispatch}, {ctx, email, password, redirect = false}) {
      Vue.http.post('/api/login', {email, password})
        .then(response => {
          commit(SET_USER, {
            ...response.body.data,
            token: response.body.meta.token,
            loggedIn: true,
          })

          if (redirect) {
            router.push({name: redirect})
          }
        }, response => {
          dispatch(ACTION_PUSH_MESSAGE, {
            msg: 'Неправильные email и/или пароль',
            type: 'danger',
          })

          getValidationErrorsFromServer.call(ctx, {response})
          pushErrorMessage.call(ctx, {response})
        })
    },
    [LOGOUT]({commit}, {redirect = false}) {
      commit(CLEAR_USER)

      if (redirect) {
        router.push({name: redirect})
      }
    },
    [TOKEN_EXPIRED]({commit, dispatch}, redirect = 'login') {
      commit(CLEAR_USER)

      if (redirect) {
        router.push({name: redirect})
      }

      dispatch(ACTION_PUSH_MESSAGE, {
        msg: 'Ваша сессия истекла, пожалуйста, войдите на сайт заново',
        type: 'info',
      })
    },
    [ACTION_PUSH_MESSAGE]({commit, state}, {msg, type}) {
      const id = getNextMessageId(state)
      commit(PUSH_MESSAGE, {msg, type, id})

      setTimeout(() => {
        commit(REMOVE_MESSAGE_BY_ID, id)
      }, 5000)
    },
  },
  getters: {
    hasRole: state => role => {
      return state.user.roles.some(r => {
        return r.name === role
      })
    },
    can: state => perm => {
      return state.user.permissions.some(p => {
        return p.name === perm
      })
    },
  },
})

export default store

function getNextMessageId(state) {
  const length = state.messages.length

  if (length > 0) {
    return state.messages[length - 1].id + 1
  } else {
    return 1
  }
}

function updateUser() {
  if (!store.state.user.loggedIn) {
    return
  }

  Vue.http.get('/api/user')
    .then(response => {
      store.commit(SET_USER, {
        ...response.body.data,
      })
    }, response => {
      store.dispatch(ACTION_PUSH_MESSAGE, {
        msg: 'Не удалось получить новые данные о текущем пользователе',
        type: 'danger',
      })
    })
}

setInterval(updateUser, 5000)

export const loadStoreFromLocalStorage = (store) => {
  const user = JSON.parse(localStorage.getItem('user'))

  if (user) {
    store.commit(SET_USER, user)
  }
}
