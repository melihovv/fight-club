window.$ = window.jQuery = require('jquery')
window.Tether = require('tether')
require('bootstrap')
window.moment = require('moment')

import Vue from 'vue'
import {sync} from 'vuex-router-sync'

import App from 'components/App'
import router from './routes'
import store, {loadStoreFromLocalStorage} from './vuex/store'
import './resource'
import './validation'

import Helper from './helper'
Vue.use(Helper)

loadStoreFromLocalStorage(store)
sync(store, router)

const app = new Vue({
  router,
  store,
  ...App,
})

app.$mount('.app')
