'use strict'

const config = require('../../../build/config')
const baseUrl = config.testing.baseUrl

module.exports = {
  'FightClub': function (browser) {
    browser
      .url(baseUrl)
      .waitForElementVisible('.container', 5000)
      .assert.title('Бойцовский клуб')
      .end()
  },
}
